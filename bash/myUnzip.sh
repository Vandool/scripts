#!/bin/bash
# It takes the absolute path of a folder 
# and unzips all the files in that folder 
# to folders with identical names like 
# the zip-files

# Check if the parameter is provided
if (($# == 0)); then
	echo "You need to provide the absolute path of the directory in which the zip-files are saved"
	echo "Usage: command [path] [Optional: prefix to be removed from the unzip path]"
	exit 1
fi

# Optionally we can also remove a suffix from the name of the unzip directory
prefix=""
if (($# == 2)); then
	prefix=$2
fi

# Require root id
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "You need run this script as root"
    exit
fi

# get the argument
path=$1

# go to the path
echo "[Going to $path ...]"
cd $path
ls -ll

# performing magic
echo "[PERFORMING MAGIC]"
for file in *.zip
do
	echo "$file"

	noPrefix=${file#"$prefix"}
	echo "$noPrefix"

	dir=${noPrefix%.*}
	echo "$dir"

	unzip $file -d $dir
done

# remove all the zip files
rm *.zip
ls -ll


