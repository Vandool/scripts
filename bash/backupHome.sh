#!/bin/bash

# This programm will make a backup of the home directory
# as well as backing up the .config directry additionally
# One should manually run This
# TODO back it up with onedrive-abbraunegg
# TODO automate the process the whole process and let it run in the background (weekly)

HOME="/home/arvand"
BACKUP_DIR="/run/media/arvand/Linux-Data/backup"
ONEDRIVE_FOLDER_PATH="$HOME/OneDrive/Apps/configs"
LOCAL_FOLDER_PATH="$HOME/Documents/configs"
CONFIG_ZIP_FILE_NAME="configs-backup.tgz"
EXTRA_FILES="$HOME/.zshrc $HOME/.ideavimrc $HOME/.bashrc $HOME/.p10k.zsh"

# Exclude directories from the home, just add to the list in the curly brackets
# EXCLUDE="{'lost+found', '.cache', '$HOME/OneDrive'}"


# Require root id
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "You need run this script as root"
    exit
fi

# Do the magic
echo "Back up process begins ..."
echo "Home is so big:\t"
du -sh /home

# Back up
rsync -a --info=progress2 --exclude="baloo" --exclude="Trash" --exclude="lost+found" --exclude=".cache" --exclude="OneDrive" /home $BACKUP_DIR

# Backup config additionally
sudo tar zcvf $BACKUP_DIR/$CONFIG_ZIP_FILE_NAME --exclude='discord/*' --exclude='plasmaConfSaver/*' --exclude='notion-app-enhanced/*' --exclude='libreoffice/*' $HOME/.config
cp $BACKUP_DIR/$CONFIG_ZIP_FILE_NAME $ONEDRIVE_FOLDER_PATH
cp $BACKUP_DIR/$CONFIG_ZIP_FILE_NAME $LOCAL_FOLDER_PATH
cp $EXTRA_FILES $ONEDRIVE_FOLDER_PATH
cp $EXTRA_FILES $LOCAL_FOLDER_PATH

# Roar
echo -e "The config files are zipped and so big:"
du -sh $BACKUP_DIR/$CONFIG_ZIP_FILE_NAME
echo -e "backed up once here:"
echo -e $BACKUP_DIR

echo -e "and here:"
echo $ONEDRIVE_FOLDER_PATH
echo $LOCAL_FOLDER_PATH
echo -e "and so big:"
du -sh $ONEDRIVE_FOLDER_PATH/$CONFIG_ZIP_FILE_NAME

echo -e "\n"
echo -e "The whole home directory and the config files has been backup here:\n$BACKUP_DIR \n"

echo "The backed up home is so big:"
du -sh $BACKUP_DIR

